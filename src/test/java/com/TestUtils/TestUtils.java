package com.TestUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class TestUtils {
	File file;
	FileReader read;
	
	public String getURLFromPropertyFile() {
		
		file = new File(System.getProperty("user.dir")+ "//Config//"+ "//Env.properties");
		
		try {
			read = new FileReader(file);
		} catch (FileNotFoundException e) {
		
			e.printStackTrace();
		}
		Properties prop = new Properties();
		try {
			prop.load(read);
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
		String Data = prop.getProperty("URL");
		return Data;
		
		
		
	}

}
