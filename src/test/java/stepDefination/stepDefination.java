package stepDefination;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.hc.core5.http.HttpConnection;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.TestUtils.TestUtils;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;




public class stepDefination extends TestUtils {
	
	
	
	WebDriver driver;

	public By AcceptButton = By.id("onetrust-accept-btn-handler");
	//ArrayList<String> all = new ArrayList<String>();
	String  l;
	ArrayList<String> ListOfTiltesTobeValidatedOnUIPage ;



	Actions actions ;
	WebElement financialServiceLocator ;
	List<WebElement> ListofUIElementtobeValidated;
	List<WebElement> elements;
	HttpURLConnection conn;
	int respCode = 200;


	@Given("User Lands on the given LexisNexis URL Page and accepts the cookies popup.")
	public void User_Lands_on_the_given_URL_Page() {
			WebDriverManager.chromedriver().setup();
	//	System.setProperty("webdriver.chrome.driver","C://Users//User//Desktop//AT_Batch_June_2022//Java-Dipangana//com.LexisNexisProject//drivers//chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("https://risk.lexisnexis.co.uk/");

		driver.manage().window().maximize();
		//	driver.switchTo().alert().accept();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(30));
		driver.findElement(AcceptButton).click();


	}


	@When("User tries click on different service UI Elements & check if its present or broken link")
	public void User_tries_click_on_certain_UI_Elements_and_Header(){
		financialServiceLocator = driver.findElement(By.xpath("(//a[@class='score-button btn-clickable-area'])[9]"));
		//this.driver = driver;
		actions = new Actions(driver);

		actions.moveToElement(financialServiceLocator).build().perform();

		ListofUIElementtobeValidated = driver.findElements(By.xpath("(//div[@class='score-center'])[9]//div[@class='caption']//div[@class='score-call-to-action']//a//following-sibling::a"));

		for(WebElement wb:ListofUIElementtobeValidated) {

			String url = wb.getAttribute("href");

			if(url == null || url.isEmpty()){
				System.out.println("URL is either not configured for anchor tag or it is empty");
				continue;
			}

			try {
				conn = (HttpURLConnection)(new URL (url).openConnection());
				conn.setRequestMethod("HEAD");
				conn.connect();
				respCode = conn.getResponseCode();

				if(respCode >= 400){
					System.out.println(url+" is a broken link");
				}
				else{
					System.out.println(url+" is a valid link");
				}



			} catch (MalformedURLException e) {

				e.printStackTrace();
			} catch (IOException e) {

				e.printStackTrace();
			}


		}


	}


	@Then("User validates if the service UI Elements are present and clickable")
	public void User_validates_if_the_UI_Elements_are_present_and_clickable() throws InterruptedException {
		//	element = driver.findElements(By.xpath("(//div[@class='score-center'])[9]//div[@class='caption']//div[@class='score-call-to-action']//a//following-sibling::a"));
		ListOfTiltesTobeValidatedOnUIPage = new ArrayList<String>();
		ListOfTiltesTobeValidatedOnUIPage.add("Financial Services | LexisNexis Risk Solutions");
		ListOfTiltesTobeValidatedOnUIPage.add("Insurance");
		ListOfTiltesTobeValidatedOnUIPage.add("Life and Pensions");
		ListOfTiltesTobeValidatedOnUIPage.add("Corporations and non Profits");


		elements = driver.findElements(By.xpath("(//div[@class='score-center'])[9]//div[@class='caption']//div[@class='score-call-to-action']//a//following-sibling::a"));



		for(int i =1;i<=elements.size();i++) {

			driver.findElement(By.xpath("((//div[@class='score-center'])[9]//div[@class='caption']//div[@class='score-call-to-action']//a//following-sibling::a)["+i+"]")).click();
			String PageTitleOfDifferentUITiles =driver.getTitle();
			for(String s :ListOfTiltesTobeValidatedOnUIPage) {
				if(s==PageTitleOfDifferentUITiles) {
					Assert.assertEquals(l,s);
					break;
				}
			}
			driver.navigate().back();
			Thread.sleep(3000);
		}
		Thread.sleep(3000);	
		driver.close();
		}


	@When("User clicks on the Choose Your Industry dropdown")
	public void User_clicks_on_the_Choose_Your_Industry_dropdown() {

		driver.findElement(By.xpath("//div[@class='score-style-box']//following-sibling::li//a[2]//span")).click();
		String header = driver.findElement(By.xpath("//div[@class='section-header']//h3")).getText();
		Assert.assertEquals(header,"Industries");
	}
	
	@Then("User clicks on financial service industry")
	public void User_clicks_on_first_industry() {
		
		driver.findElement(By.xpath("//ul[@role='tablist']//li[1]")).click();
		String SelectanIndustryHeadertext = driver.findElement(By.xpath("(//div[@class='second-step']//li[2]//span)[1]")).getText();
		Assert.assertEquals(SelectanIndustryHeadertext, "Select an Industry");
		
	}
	
	@And("User validates the other services under financial service industry")
	public void User_validates_the_other_industries() throws InterruptedException {
		//(//div[@class='second-step']//ul//following-sibling::li//following-sibling::li//a)[1]
		driver.findElement(By.xpath("(//div[@class='second-step']//ul//following-sibling::li//following-sibling::li//a)[1]")).click();
		Thread.sleep(3000);
		String selectLinkOpeninNewTab = Keys.chord(Keys.CONTROL,Keys.RETURN); 
		//driver.findElement(By.linkText("www.facebook.com")).sendKeys(selectLinkOpeninNewTab);
		for(int i=1;i<7;i++) {
			driver.findElement(By.xpath("(//div[@class='section-title-wraper']//following-sibling::ul//li//a)["+i+"]")).sendKeys(selectLinkOpeninNewTab);
		}
		
		Thread.sleep(3000);
		
		ArrayList<String> ListofWindowHeaders = new ArrayList<String>();
		ListofWindowHeaders.add("LexisNexis Risk Solutions | Transform Your Risk Decision Making");
		ListofWindowHeaders.add("Investigations and Due Diligence | LexisNexis Risk Solutions");
		ListofWindowHeaders.add("Collections and Recovery | LexisNexis Risk Solutions");
		ListofWindowHeaders.add("Credit Risk Assessment | LexisNexis Risk Solutions");
		ListofWindowHeaders.add("Fraud and Identity Management | LexisNexis Risk Solutions");
		ListofWindowHeaders.add("Financial Crime Compliance | LexisNexis Risk Solutions");
		ListofWindowHeaders.add("Customer Data Management | LexisNexis Risk Solutions");
		

		Set<String> windowHandles = driver.getWindowHandles();
		ArrayList<String> ListofpageTitles = new ArrayList<String>();
		Iterator<String> it =windowHandles.iterator();
		while(it.hasNext()) {
			
			driver.switchTo().window(it.next());
			ListofpageTitles.add(driver.getTitle());
			//System.out.println(driver.getTitle());
		}
		
		for(int i =0;i<ListofpageTitles.size();i++) {
			
			//System.out.println(ListofWindowHeaders.get(i));
			
			Assert.assertEquals(ListofpageTitles.get(i), ListofWindowHeaders.get(i));
			
			
		}
		
	//	System.out.println(all.toString());
		
		driver.close();
	}
	

}




















